package waybackcsv

import (
	"os"
	"bufio"
	"strings"
	"strconv"
	"io/ioutil"
	"encoding/json"
	"../communication"
)

type WBReader struct {
	File string
	From uint64
	To uint64
	All bool
	num uint64
}

func NewReader(file string) *WBReader {
	return &WBReader{
		File: file,
		From: 0,
		To: 0,
		num: 0,
	}
}

func (wb *WBReader) ReadCfg(cfg string) {
	file, err := ioutil.ReadFile(cfg)

	if err != nil {
			panic("cant read cfg file")
	}

	jsonCfg := make(map[string]interface{})

	err = json.Unmarshal(file, &jsonCfg)

	if err != nil {
		panic("cant read json")
	}

	value := jsonCfg["from"].(string)
	from, _ := strconv.ParseUint(value, 10, 64)

	value = jsonCfg["len"].(string)
	to, _ := strconv.ParseUint(value, 10, 64)

	all := false

	if jsonCfg["all"].(string) == "true" {
		all = true
	}

	wb.From = from
	wb.To = from + to
	wb.All = all
}

func (wb *WBReader) WriteIn(channel chan<- interface{}) {
	file, err := os.Open(wb.File)

	if err != nil {
		panic("cant open csv file")
	}

	defer file.Close()

	scanner := bufio.NewScanner(file)

	for wb.num < wb.From {
		scanner.Scan()
		wb.num++
	}

	for scanner.Scan() {
		if wb.All || wb.num < wb.To {
			line := scanner.Text()
			fields := strings.Split(line, ",")

			if len(fields) < 2 {
				continue
			}

			var date uint64 = 0

			topaddress := fields[0]
			address := fields[1]

			record := communication.NewDataCluster(date, address, topaddress)

			channel <- record
		} else {
			break
		}

		wb.num++
	}
}
