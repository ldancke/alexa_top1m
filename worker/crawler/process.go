package crawler

import (
	"net/url"
	"strings"
	"fmt"
	"regexp"
	"hash/fnv"
)

type Processor struct {
	Links []*url.URL
	EmbeddedScripts []string
	hash uint32
}

func NewProcessor() Processor {
	return Processor{
		Links: make([]*url.URL, 0),
		EmbeddedScripts: make([]string, 0),
		hash: 0,
	}
}

func Hash(text string) uint32 {
	hash := fnv.New32a()
	hash.Write([]byte(text))
	return hash.Sum32()
}

func (pro *Processor) ExtractJs(webpage *Webpage) {
	if webpage.Ext == "js" {
		return
	}

	regex := regexp.MustCompile(`<script.*?(?:src=(?:'|")(.+?)(?:'|").*?)?(?:\/>|>([\s\S]*?)<\/script>)`)
	matches := regex.FindAllStringSubmatch(webpage.Content, -1)

	for _, match := range matches {

		if match[1] != "" {
			pro.HandleLink(webpage, match[1])
		} else {
			pro.HandleEmbeddedScript(match[2])
		}

		replacement := fmt.Sprintf(`<script src="%d.js" />`, pro.hash)
		webpage.Content = strings.Replace(webpage.Content, match[0], replacement, 1)
	}
}

func (pro *Processor) HandleLink(webpage *Webpage, link string) {
	pro.hash = Hash(link)

	newURL, err := url.Parse(link)
	if err != nil {
		return
	}

	pro.Links = append(pro.Links, webpage.Address.ResolveReference(newURL))
}

func (pro *Processor) HandleEmbeddedScript(embeddedScript string) {
	pro.hash = Hash(embeddedScript)
	pro.EmbeddedScripts = append(pro.EmbeddedScripts, embeddedScript)
}
