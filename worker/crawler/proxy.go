package crawler

import (
	"net/url"
	"math/rand"
	"sync"
)

type ProxyList struct {
	List []Proxy
	lock sync.Mutex
}

type Proxy struct {
	URL *url.URL
	Fails uint32
	Use uint32
	Index int
}

func NewProxyList(len int) ProxyList {
	return ProxyList{
		List: make([]Proxy, len),
		lock: sync.Mutex{},
	}
}

func (pl *ProxyList) SelectRandom() *Proxy {
	pl.lock.Lock()
	defer pl.lock.Unlock()

	if len(proxies.List) < 2 {
		return &pl.List[len(pl.List) - 1]
	}

	numA := rand.Intn(len(pl.List))
	numB := rand.Intn(len(pl.List))

	failA := float64(pl.List[numA].Fails) / float64(pl.List[numA].Use)
	failB := float64(pl.List[numB].Fails) / float64(pl.List[numB].Use)

	var ret *Proxy

	if failA < failB {
		pl.List[numA].Use = pl.List[numA].Use + 1
		ret = &pl.List[numA]
	} else {
		pl.List[numB].Use = pl.List[numB].Use + 1
		ret = &pl.List[numB]
	}

	return ret
}

func (pl *ProxyList) UpdateFailed(proxy *Proxy) {
	pl.lock.Lock()
	defer pl.lock.Unlock()

	pl.List[proxy.Index].Fails = pl.List[proxy.Index].Fails + 1
}
