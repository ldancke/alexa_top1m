package crawler

import (
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
	"time"
	"math/rand"
	"encoding/json"
	"errors"
)

type Crawler struct {
	Client *http.Client
	Transport *http.Transport
}

var (
	ErrPanic = errors.New("Crawler - Panic error")
	ErrTimeout = errors.New("Crawler - Timeout error")
	ErrOther = errors.New("Crawler - Other error")
	ErrHttpFailed = errors.New("HTTP - Request failed")
	ErrHttpTimeout = errors.New("HTTP - Request timeout")
	ErrHttpRequest = errors.New("HTTP - Request error")
	ErrStatusFailed = errors.New("HTTP - Download failed")
	ErrStatus404 = errors.New("HTTP - Page not found")
	ErrReadBody = errors.New("Response - Content is not readable")
)

var (
	path string
	proxies ProxyList
)

func init() {
	rand.Seed(time.Now().Unix())
}

func ReadCfg(cfg string) {
	file, err := ioutil.ReadFile(cfg)

	if err != nil {
		panic("cant read cfg file")
	}

	jsonCfg := make(map[string]interface{})

	err = json.Unmarshal(file, &jsonCfg)

	if err != nil {
		panic("cant read json")
	}

	path = jsonCfg["directory"].(string)

	fields := strings.Split(jsonCfg["proxy"].(string), ",")

	proxies = NewProxyList(len(fields) + 1)

	for i, p := range fields {
		proxy, err := url.Parse(p)
		if err != nil {
			panic("Could not parse proxy: "+p)
		}

		proxies.List[i] = Proxy{
				URL: proxy,
				Fails: 0,
				Use: 0,
				Index: i,
			}
	}

	proxies.List[len(proxies.List) - 1] = Proxy{
		URL: nil,
		Fails: 0,
		Use: 0,
		Index: len(proxies.List) - 1,
	}
}

func NewCrawler() *Crawler {
	return &Crawler{
		Client: &http.Client{},
		Transport: &http.Transport{},
	}
}

func (cw *Crawler) Connect() {
	cw.Client.Transport = cw.Transport
}

func (cw *Crawler) DisableKeepAlives(disable bool) {
	cw.Transport.DisableKeepAlives = disable
}

func (cw *Crawler) Proxy(proxy *url.URL) {
	if proxy != nil {
		cw.Transport.Proxy = http.ProxyURL(proxy)
	}
}

func (cw *Crawler) Timeout(timeout uint32) {
	cw.Client.Timeout = time.Duration(timeout) * time.Second
}

func (cw *Crawler) Download(page *Webpage) error {

	cw.Timeout(page.Timeout)

	httpReq, err := http.NewRequest(http.MethodGet, page.Address.String(), nil)

	if err != nil {
		return ErrHttpFailed
	}

	httpReq.Close = true

	httpRes, err := cw.Client.Do(httpReq)

	if err != nil {
		if strings.Contains(err.Error(), "Timeout") {
			return ErrHttpTimeout
		} else {
			return ErrHttpRequest
		}
	}

	defer httpRes.Body.Close()

	switch httpRes.StatusCode {
	case 200, 201:
	case 404:
		return ErrStatus404
	default:
		return ErrStatusFailed
	}

	media := httpRes.Header.Get("Content-Type")
	content, err := ioutil.ReadAll(httpRes.Body)

	if err != nil {
		return ErrReadBody
	}

	page.Content = string(content)
	page.Media = media
	page.ExtensionFromMedia()

	return nil
}

func (cw *Crawler) Crawl(pages []*Webpage) []*Webpage {
	var err error
	var touched []*Webpage

	for _, page := range pages {
		touched = append(touched, page)

		page.HashAddress()

		if cw.Exists(page) {
			page.Stored = true
			continue
		}

		if len(page.Address.Path) > 2048 {
			continue
		}

		err = cw.Download(page)

		switch err {
		case nil:
		case ErrHttpTimeout:
			page.Err = ErrTimeout
			continue
		case ErrHttpRequest, ErrReadBody:
			page.Err = ErrOther
			continue
		default:
			page.Err = ErrPanic
			continue
		}

		processor := NewProcessor()
		processor.ExtractJs(page)

		for _, link := range processor.Links {
			processed := NewWebpage()
			processed.Date = page.Date
			processed.OriginAddress = page.OriginAddress
			processed.Address = link
			processed.Extracted = true

			touched = append(touched, &processed)
		}

		//page.AddMeta()

		err = cw.Store(page)

		if err != nil {
			page.Err = ErrOther
			continue
		}

		page.Stored = true
	}

	return touched
}
