package crawler

import (
	"net/url"
	"errors"
	"strings"
)

type Webpage struct {
	OriginAddress *url.URL
	Address *url.URL
	Proxy *url.URL
	Date uint64
	Content string
	Media string
	Ext string
	Timeout uint32
	Hash uint32
	Err error
	Extracted bool
	Stored bool
}

const (
	ExtHTML = ".html"
	ExtPHP = ".php"
	ExtJS = ".js"
)

var (
	ErrParsing = errors.New("Address - Url cannot be parsed")
	ErrHostname = errors.New("Address - Url cannot be parsed")
	ErrSandbox = errors.New("Sandbox - Url cannot be parsed")
	ErrPageURL = errors.New("Webpage - Url cannot be parsed")
)

func NewWebpage() Webpage {
	return Webpage{
		OriginAddress: nil,
		Address: nil,
		Date: 0,
		Content: "",
		Media: "",
		Ext: "",
		Timeout: 0,
		Hash: 0,
		Err: nil,
		Stored: false,
	}
}

func (wp *Webpage) HashAddress() {
	wp.Hash = Hash(wp.Address.String())
}

func (wp *Webpage) ParseAddress(address string) error {
	parsed, err := url.Parse(address)

	if err != nil {
		return ErrPageURL
	}

	wp.Address = parsed
	return nil
}

func (wp *Webpage) ExtensionFromMedia() {
	switch {
	case strings.Contains(wp.Media, "html"):
		wp.Ext = ExtHTML
	case strings.Contains(wp.Media, "javascript"):
		wp.Ext = ExtJS
	case strings.Contains(wp.Media, "php"):
		wp.Ext = ExtPHP
	}
}
